package tsm.load.sender

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
object Main extends App {
     implicit val system = ActorSystem("load-sender")
     implicit val timeout = Timeout(5.seconds)
     Console.println("Hello World!")

     val service = system.actorOf(Props[ServiceRounter])

     // start a new HTTP server on port 8080 with our service actor as the handler
     IO(Http) ? Http.Bind(service, interface = "localhost", port = 8090)

 }
// vim: set ts=2 sw=2 et:



