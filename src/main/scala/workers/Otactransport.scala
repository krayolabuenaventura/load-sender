package tsm.load.sender.worker


// vim: set ts=2 sw=2 et:
import akka.actor.Props
import akka.actor.ActorLogging
import akka.actor.Actor
import tsm.load.sender.model.Command
import spray.routing.RequestContext
// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class OtacTransport(ctx: RequestContext) extends Actor with ActorLogging {
	def receive = {
		case msg @ Command(cmd) => println("eee ->" + cmd)
		ctx.complete("200")
		case _ =>

	}
}

object OtacTransport {
	def props(ctx: RequestContext): Props = Props(new OtacTransport(ctx))
}