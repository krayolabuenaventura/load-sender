package tsm.load.sender

import tsm.load.sender.model._
import tsm.load.sender.worker._
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import spray.routing._
import spray.http._
import MediaTypes._
import spray.routing.RequestContext

import tsm.load.sender._
// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class ServiceRounter extends Actor with Router with ActorLogging {
def requestHandler(ctx: RequestContext) = context.actorOf(OtacTransport.props(ctx))
  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(myRoute)
}


// this trait defines our service behavior independently from the service actor
trait Router extends HttpService {
 import EntityJsonFormat._
   def requestHandler(ctx: RequestContext): ActorRef
  val myRoute =
    path("sendload") {
  logRequestResponse("get-user")
    post {
      entity(as[Command]) { entity =>
        ctx =>
        
        requestHandler(ctx) ! Command(entity.command)
      }
    }
    }
}