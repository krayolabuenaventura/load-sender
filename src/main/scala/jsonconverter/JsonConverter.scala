package tsm.load.sender
import tsm.load.sender.model._
import spray.json._
import DefaultJsonProtocol._
import spray.json.JsonFormat
import spray.httpx.Json4sSupport
import org.json4s.Formats
import org.json4s.DefaultFormats
import spray.httpx.SprayJsonSupport

object Json4sProtocol extends Json4sSupport {
  implicit def json4sFormats: Formats = DefaultFormats
}

object EntityJsonFormat extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val jsObjFormatFileHeader = jsonFormat1(Command)
}
// vim: set ts=2 sw=2 et:
